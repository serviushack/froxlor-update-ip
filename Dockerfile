# OpenSSL 1.0 (used in clux/muslrust) fails on expired intermediate CAs
# Remove the CA from cert.pem
FROM alpine as addtrust-fix
RUN apk add curl
RUN curl https://curl.se/ca/cacert.pem | sed '/AddTrust External Root/,/-----END CERTIFICATE-----/d' > /cacert.pem

FROM scratch
COPY target/x86_64-unknown-linux-musl/release/froxlor_update_ip /
COPY --chown=1000:1000 --from=addtrust-fix /cacert.pem /etc/ssl/cert.pem
USER 1000
CMD ["/froxlor_update_ip"]
