use std::env;
use std::error::Error;
use std::fmt;
use std::time::Duration;

#[derive(PartialEq)]
pub enum IPVersions {
    IPv4,
    IPv6,
    Both,
}

pub struct Config {
    pub interval: Duration,
    pub apikey: String,
    pub secret: String,
    pub froxlor_url: String,
    pub domain: String,
    pub record_names: Vec<String>,
    pub ttl: String,
    pub ip_versions: IPVersions,
}

#[derive(Debug)]
pub struct MissingVariablesError {
    variables: Vec<String>,
}

impl fmt::Display for MissingVariablesError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "The following environment variables are missing: {}",
            self.variables.join(" ")
        )
    }
}

impl Error for MissingVariablesError {}

#[derive(Debug)]
pub struct ParseError {
    variable: String,
    error: String,
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Failed parsing the content of {}: {}",
            self.variable, self.error
        )
    }
}

impl Error for ParseError {}

pub fn from_env() -> Result<Config, Box<dyn Error>> {
    let mut missing_variables = Vec::new();

    let apikey = env::var("APIKEY");
    if let Err(_) = apikey {
        missing_variables.push("APIKEY".to_string());
    }

    let secret = env::var("SECRET");
    if let Err(_) = secret {
        missing_variables.push("SECRET".to_string());
    }

    let froxlor = env::var("FROXLOR");
    if let Err(_) = froxlor {
        missing_variables.push("FROXLOR".to_string());
    }

    let domain = env::var("DOMAIN");
    if let Err(_) = domain {
        missing_variables.push("DOMAIN".to_string());
    }

    let mut record_names = Vec::new();
    let provided_record_name = env::var("RECORD_NAME");
    let provided_record_names = env::var("RECORD_NAMES");
    if provided_record_name.is_err() && provided_record_names.is_err() {
        missing_variables.push("RECORD_NAME or RECORD_NAMES".to_string());
    } else if provided_record_name.is_ok() {
        record_names.push(provided_record_name.unwrap());
    } else if provided_record_names.is_ok() {
        record_names = provided_record_names
            .unwrap()
            .split(",")
            .map(|s| s.to_string())
            .collect();
    }

    let ttl = env::var("TTL").unwrap_or("1800".to_string());

    let interval = match env::var("INTERVAL")
        .unwrap_or("600".to_string())
        .parse::<u64>()
    {
        Err(err) => {
            return Err(Box::new(ParseError {
                variable: "INTERVAL".to_string(),
                error: err.to_string(),
            }))
        }
        Ok(value) => Duration::from_secs(value),
    };

    let ip_versions = match env::var("IPV").unwrap_or("4".to_string()).as_str() {
        "4" => IPVersions::IPv4,
        "6" => IPVersions::IPv6,
        "both" => IPVersions::Both,
        _ => {
            return Err(Box::new(ParseError {
                variable: "IPV".to_string(),
                error: "Must be one of '4', '6' or 'both'.".to_string(),
            }))
        }
    };

    if !missing_variables.is_empty() {
        return Err(Box::new(MissingVariablesError {
            variables: missing_variables,
        }));
    }

    Ok(Config {
        interval: interval,
        apikey: apikey.unwrap(),
        secret: secret.unwrap(),
        froxlor_url: format!("{}/api.php", froxlor.unwrap()),
        domain: domain.unwrap(),
        record_names: record_names,
        ttl: ttl,
        ip_versions: ip_versions,
    })
}
