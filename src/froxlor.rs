use serde::{Deserialize, Serialize};
use serde_json::json;
use std::io::Read;

pub struct Froxlor {
    header: Header,
    url: String,
}

#[derive(Serialize)]
struct Header {
    apikey: String,
    secret: String,
}

#[derive(Deserialize)]
struct GenericResponse {
    status: u16,
}

#[derive(Deserialize)]
struct ListingResponse {
    status: u16,
    data: ListingResponseData,
}

#[derive(Deserialize)]
struct ListingResponseData {
    list: Vec<ListingResponseRecord>,
}

#[derive(Deserialize)]
struct ListingResponseRecord {
    id: String,
    r#type: String,
}

pub struct Record {
    pub id: String,
    pub record_type: String,
}

impl Froxlor {
    pub fn new(froxlor_url: String, apikey: String, secret: String) -> Froxlor {
        return Froxlor {
            header: Header {
                apikey: apikey,
                secret: secret,
            },
            url: froxlor_url,
        };
    }

    pub fn find_record(
        &self,
        domain: &String,
        record_name: &String,
    ) -> Result<Vec<Record>, Box<dyn std::error::Error>> {
        let listing = json!({
            "header": self.header,
            "body": {
                "command": "DomainZones.listing",
                "params": {
                    "domainname": domain,
                    "sql_search": {
                        "record": { "op": "=", "value": record_name }
                    }
                }
            }
        });

        let client = reqwest::blocking::Client::new();
        let mut res = client.post(&self.url).json(&listing).send()?;

        if res.status() != 200 {
            let mut body = String::new();
            res.read_to_string(&mut body)?;
            return Err(body.into());
        }

        let response = res.json::<ListingResponse>()?;

        if response.status != 200 {
            return Err(
                (format!("Inner status code is {} instead of 200.", response.status)).into(),
            );
        }

        return Ok(response
            .data
            .list
            .into_iter()
            .map(|x| Record {
                id: x.id.clone(),
                record_type: x.r#type.clone(),
            })
            .collect());
    }

    pub fn delete_record(
        &self,
        domain: &String,
        entry_id: &String,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let delete = json!({
            "header": self.header,
            "body": {
                "command": "DomainZones.delete",
                "params": {
                    "domainname": domain,
                    "entry_id": entry_id
                }
            }
        });

        let client = reqwest::blocking::Client::new();
        let mut res = client.post(&self.url).json(&delete).send()?;

        if res.status() != 200 {
            let mut body = String::new();
            res.read_to_string(&mut body)?;
            return Err(body.into());
        }

        let response = res.json::<GenericResponse>()?;

        if response.status == 200 {
            return Ok(());
        } else {
            return Err(
                (format!("Inner status code is {} instead of 200.", response.status)).into(),
            );
        }
    }

    pub fn add_record(
        &self,
        domain: &String,
        record_name: &String,
        content: &String,
        ttl: &String,
        record_type: &String,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let add = json!({
            "header": self.header,
            "body": {
                "command": "DomainZones.add",
                "params": {
                    "domainname": domain,
                    "record": record_name,
                    "content": content,
                    "ttl": ttl,
                    "type": record_type,
                }
            }
        });

        let client = reqwest::blocking::Client::new();
        let mut res = client.post(&self.url).json(&add).send()?;

        if res.status() != 200 {
            let mut body = String::new();
            res.read_to_string(&mut body)?;
            return Err(body.into());
        }

        let response = res.json::<GenericResponse>()?;

        if response.status == 200 {
            return Ok(());
        } else {
            return Err(
                (format!("Inner status code is {} instead of 200.", response.status)).into(),
            );
        }
    }
}
