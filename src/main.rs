extern crate crossbeam_channel;
extern crate ctrlc;
extern crate reqwest;

mod config;
mod external_ip;
mod froxlor;
mod logic;

use crossbeam_channel::{bounded, select, tick, Receiver};

fn setup_ctrlc() -> Result<Receiver<()>, ctrlc::Error> {
    let (sender, receiver) = bounded(1);
    ctrlc::set_handler(move || {
        let _ = sender.send(());
    })?;

    Ok(receiver)
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let stop_events = setup_ctrlc()?;

    let config = config::from_env()?;
    let froxlor = froxlor::Froxlor::new(config.froxlor_url, config.apikey, config.secret);

    let mut logic = logic::Logic::new(
        froxlor,
        config.domain,
        config.record_names,
        config.ttl,
        config.ip_versions,
    );

    loop {
        logic.update()?;

        let tick = tick(config.interval);
        select! {
            recv(tick) -> _ => { }
            recv(stop_events) -> _ => {
                return Ok(());
            }
        }
    }
}
