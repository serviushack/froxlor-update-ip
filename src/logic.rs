use crate::external_ip;
use crate::froxlor;

use crate::config::IPVersions;

pub struct Logic {
    previous_ips: Option<external_ip::ExternalIPs>,
    froxlor: froxlor::Froxlor,
    domain: String,
    record_names: Vec<String>,
    ttl: String,
    ip_versions: IPVersions,
}

impl Logic {
    pub fn new(
        froxlor: froxlor::Froxlor,
        domain: String,
        record_names: Vec<String>,
        ttl: String,
        ip_versions: IPVersions,
    ) -> Logic {
        Logic {
            previous_ips: None,
            froxlor: froxlor,
            domain: domain,
            record_names: record_names,
            ttl: ttl,
            ip_versions: ip_versions,
        }
    }

    pub fn update(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        let ips = external_ip::determine(&self.ip_versions)?;

        if let Some(ref ip) = ips.ipv4 {
            println!("External IPv4: {}", ip);
        };
        if let Some(ref ip) = ips.ipv6 {
            println!("External IPv6: {}", ip)
        };

        if self.previous_ips.is_none() || self.previous_ips.as_ref().unwrap() != &ips {
            for record_name in &self.record_names {
                match self.froxlor.find_record(&self.domain, &record_name) {
                    Err(error) => return Err(error),
                    Ok(entries) => {
                        for entry in entries.into_iter().filter(|entry| {
                            if entry.record_type == "A" {
                                return self.ip_versions == IPVersions::IPv4
                                    || self.ip_versions == IPVersions::Both;
                            }
                            if entry.record_type == "AAAA" {
                                return self.ip_versions == IPVersions::IPv6
                                    || self.ip_versions == IPVersions::Both;
                            }

                            return false;
                        }) {
                            match self.froxlor.delete_record(&self.domain, &entry.id) {
                                Err(error) => return Err(error),
                                Ok(_) => (),
                            }
                        }
                    }
                };

                if let Some(ref ip) = ips.ipv4 {
                    match self.froxlor.add_record(
                        &self.domain,
                        &record_name,
                        &ip.to_string(),
                        &self.ttl,
                        &"A".to_string(),
                    ) {
                        Err(error) => return Err(error),
                        Ok(_) => (),
                    }
                };

                if let Some(ref ip) = ips.ipv6 {
                    match self.froxlor.add_record(
                        &self.domain,
                        &record_name,
                        &ip.to_string(),
                        &self.ttl,
                        &"AAAA".to_string(),
                    ) {
                        Err(error) => return Err(error),
                        Ok(_) => (),
                    }
                };
            }

            self.previous_ips = Some(ips);
        }

        return Ok(());
    }
}

#[cfg(test)]
mod tests {
    use crate::config::IPVersions;
    use crate::froxlor;
    use mockito::{mock, Matcher, Mock};
    use serde_json::json;

    static API_KEY: &str = "api-key";
    static SECRET: &str = "secret";
    static DOMAIN: &str = "domain";

    fn create_external_ip_mock(address: &str) -> Mock {
        return mock("GET", "/")
            .with_status(200)
            .with_body(address)
            .create();
    }

    fn create_listing_request_mock(entries: Vec<froxlor::Record>) -> Mock {
        let request_body = json!({
            "header": {
                "apikey": API_KEY,
                "secret": SECRET
            },
            "body": {
                "command": "DomainZones.listing",
                "params": {
                    "domainname": "domain",
                    "sql_search": {
                        "record": { "op": "=", "value": "record1" }
                    }
                }
            }
        });

        let list: Vec<serde_json::Value> = entries
            .into_iter()
            .map(|entry| {
                let mut m = serde_json::Map::new();
                m.insert(
                    "id".to_string(),
                    serde_json::Value::String(entry.id.to_string()),
                );
                m.insert(
                    "type".to_string(),
                    serde_json::Value::String(entry.record_type.to_string()),
                );
                return serde_json::Value::Object(m);
            })
            .collect();
        let response_body = json!({
        "status": 200,
        "data": {
            "count": list.len(),
            "list": list
        }
            })
        .to_string();

        return mock("POST", "/froxlor")
            .match_body(Matcher::Json(request_body))
            .with_status(200)
            .with_body(response_body)
            .create();
    }

    fn create_delete_request_mock(entry_id: &str) -> Mock {
        let request_body = json!({
            "header": {
                "apikey": API_KEY,
                "secret": SECRET
            },
            "body": {
                "command": "DomainZones.delete",
                "params": {
                    "domainname": DOMAIN,
                    "entry_id": entry_id
                }
            }
        });

        let response_body = json!({
        "status": 200,
        })
        .to_string();

        return mock("POST", "/froxlor")
            .match_body(Matcher::Json(request_body))
            .with_status(200)
            .with_body(response_body)
            .create();
    }

    fn create_add_request_mock(record: &str, content: &str, ttl: &str, record_type: &str) -> Mock {
        let request_body = json!({
            "header": {
                "apikey": API_KEY,
                "secret": SECRET
            },
            "body": {
                "command": "DomainZones.add",
                "params": {
                    "domainname": DOMAIN,
                    "record": record,
                    "content": content,
                    "ttl": ttl,
                    "type": record_type,
                }
            }
        });

        let response_body = json!({
        "status": 200,
        })
        .to_string();

        return mock("POST", "/froxlor")
            .match_body(Matcher::Json(request_body))
            .with_status(200)
            .with_body(response_body)
            .create();
    }

    #[test]
    /// Given: No existing records
    /// When: IPv4 address is determined
    /// Then: A record is created
    fn test_initial_ipv4() {
        let record_names = vec![String::from("record1")];
        let ttl = "ttl";
        let froxlor = froxlor::Froxlor::new(
            mockito::server_url() + "/froxlor",
            API_KEY.to_string(),
            SECRET.to_string(),
        );
        let mut update = super::Logic::new(
            froxlor,
            DOMAIN.to_string(),
            record_names,
            ttl.to_string(),
            IPVersions::IPv4,
        );

        let mock_external_ip = create_external_ip_mock("127.1.2.3");
        let mock_listing = create_listing_request_mock(vec![]);
        let mock_add_v4 = create_add_request_mock("record1", "127.1.2.3", "ttl", "A");

        update.update().unwrap();

        mock_external_ip.assert();
        mock_listing.assert();
        mock_add_v4.assert();
    }

    #[test]
    /// Given: No existing records
    /// When: IPv6 address is determined
    /// Then: AAAA record is created
    fn test_initial_ipv6() {
        let record_names = vec![String::from("record1")];
        let ttl = "ttl";
        let froxlor = froxlor::Froxlor::new(
            mockito::server_url() + "/froxlor",
            API_KEY.to_string(),
            SECRET.to_string(),
        );
        let mut update = super::Logic::new(
            froxlor,
            DOMAIN.to_string(),
            record_names,
            ttl.to_string(),
            IPVersions::IPv6,
        );

        let mock_external_ip = create_external_ip_mock("adca:3897:2411:9a3e:eb2e:70cb:b7be:1d60");
        let mock_listing = create_listing_request_mock(vec![]);
        let mock_add_v6 = create_add_request_mock(
            "record1",
            "adca:3897:2411:9a3e:eb2e:70cb:b7be:1d60",
            "ttl",
            "AAAA",
        );

        update.update().unwrap();

        mock_external_ip.assert();
        mock_listing.assert();
        mock_add_v6.assert();
    }

    #[test]
    /// Given: No existing records
    /// When: IPv4 and IPv6 address is determined
    /// Then: A record and AAAA record is created
    fn test_initial_ipv4_and_ipv6() {
        let record_names = vec![String::from("record1")];
        let ttl = "ttl";
        let froxlor = froxlor::Froxlor::new(
            mockito::server_url() + "/froxlor",
            API_KEY.to_string(),
            SECRET.to_string(),
        );
        let mut update = super::Logic::new(
            froxlor,
            DOMAIN.to_string(),
            record_names,
            ttl.to_string(),
            IPVersions::Both,
        );

        let mock_external_ipv4 = create_external_ip_mock("127.1.2.3");
        let mock_external_ipv6 = create_external_ip_mock("adca:3897:2411:9a3e:eb2e:70cb:b7be:1d60");
        let mock_listing = create_listing_request_mock(vec![]);
        let mock_add_v4 = create_add_request_mock("record1", "127.1.2.3", "ttl", "A");
        let mock_add_v6 = create_add_request_mock(
            "record1",
            "adca:3897:2411:9a3e:eb2e:70cb:b7be:1d60",
            "ttl",
            "AAAA",
        );

        update.update().unwrap();

        mock_external_ipv4.assert();
        mock_external_ipv6.assert();
        mock_listing.assert();
        mock_add_v4.assert();
        mock_add_v6.assert();
    }

    #[test]
    /// Given: A record with IPv4 address
    /// When: IPv6 address is determined as external IP as well
    /// Then: A record is deleted and A and AAAA records are created
    fn test_emerged_ipv6() {
        let record_names = vec![String::from("record1")];
        let ttl = "ttl";
        let froxlor = froxlor::Froxlor::new(
            mockito::server_url() + "/froxlor",
            API_KEY.to_string(),
            SECRET.to_string(),
        );
        let mut update = super::Logic::new(
            froxlor,
            DOMAIN.to_string(),
            record_names,
            ttl.to_string(),
            IPVersions::Both,
        );

        let mock_external_ip_v4 = create_external_ip_mock("127.1.2.3");
        let mock_external_ip_v6 =
            create_external_ip_mock("7ea2:635c:4f69:e99b:29b2:3d31:677f:e3d4");
        let mock_listing = create_listing_request_mock(vec![
            froxlor::Record {
                id: "123".to_string(),
                record_type: "A".to_string(),
            },
            froxlor::Record {
                id: "124".to_string(),
                record_type: "TXT".to_string(),
            },
        ]);
        let mock_delete = create_delete_request_mock("123");
        let mock_add_v4 = create_add_request_mock("record1", "127.1.2.3", "ttl", "A");
        let mock_add_v6 = create_add_request_mock(
            "record1",
            "7ea2:635c:4f69:e99b:29b2:3d31:677f:e3d4",
            "ttl",
            "AAAA",
        );

        update.update().unwrap();

        mock_external_ip_v4.assert();
        mock_external_ip_v6.assert();
        mock_listing.assert();
        mock_delete.assert();
        mock_add_v4.assert();
        mock_add_v6.assert();
    }

    #[test]
    /// Given: A and AAAA records
    /// When: IPv4 and IPv6 addresss are determined
    /// Then: Both records are deleted and recreated
    fn test_both_updated() {
        let record_names = vec![String::from("record1")];
        let ttl = "ttl";
        let froxlor = froxlor::Froxlor::new(
            mockito::server_url() + "/froxlor",
            API_KEY.to_string(),
            SECRET.to_string(),
        );
        let mut update = super::Logic::new(
            froxlor,
            DOMAIN.to_string(),
            record_names,
            ttl.to_string(),
            IPVersions::Both,
        );

        let mock_external_ip_v4 = create_external_ip_mock("127.1.2.3");
        let mock_external_ip_v6 =
            create_external_ip_mock("7ea2:635c:4f69:e99b:29b2:3d31:677f:e3d4");
        let mock_listing = create_listing_request_mock(vec![
            froxlor::Record {
                id: "123".to_string(),
                record_type: "A".to_string(),
            },
            froxlor::Record {
                id: "124".to_string(),
                record_type: "AAAA".to_string(),
            },
        ]);
        let mock_delete_v4 = create_delete_request_mock("123");
        let mock_delete_v6 = create_delete_request_mock("124");
        let mock_add_v4 = create_add_request_mock("record1", "127.1.2.3", "ttl", "A");
        let mock_add_v6 = create_add_request_mock(
            "record1",
            "7ea2:635c:4f69:e99b:29b2:3d31:677f:e3d4",
            "ttl",
            "AAAA",
        );

        update.update().unwrap();

        mock_external_ip_v4.assert();
        mock_external_ip_v6.assert();
        mock_listing.assert();
        mock_delete_v4.assert();
        mock_delete_v6.assert();
        mock_add_v4.assert();
        mock_add_v6.assert();
    }

    #[test]
    /// Given: only a TXT record
    /// When: IPv6 address is determined
    /// Then: no record is deleted and AAAA record is created
    fn test_foreign_record_type() {
        let record_names = vec![String::from("record1")];
        let ttl = "ttl";
        let froxlor = froxlor::Froxlor::new(
            mockito::server_url() + "/froxlor",
            API_KEY.to_string(),
            SECRET.to_string(),
        );
        let mut update = super::Logic::new(
            froxlor,
            DOMAIN.to_string(),
            record_names,
            ttl.to_string(),
            IPVersions::IPv6,
        );

        let mock_external_ip_v6 =
            create_external_ip_mock("7ea2:635c:4f69:e99b:29b2:3d31:677f:e3d4");
        let mock_listing = create_listing_request_mock(vec![froxlor::Record {
            id: "124".to_string(),
            record_type: "TXT".to_string(),
        }]);
        let mock_add_v6 = create_add_request_mock(
            "record1",
            "7ea2:635c:4f69:e99b:29b2:3d31:677f:e3d4",
            "ttl",
            "AAAA",
        );

        update.update().unwrap();

        mock_external_ip_v6.assert();
        mock_listing.assert();
        mock_add_v6.assert();
    }

    #[test]
    fn test_no_change_on_same_ip() {
        let record_names = vec![String::from("record1")];
        let ttl = "ttl";
        let froxlor = froxlor::Froxlor::new(
            mockito::server_url() + "/froxlor",
            API_KEY.to_string(),
            SECRET.to_string(),
        );
        let mut logic = super::Logic::new(
            froxlor,
            DOMAIN.to_string(),
            record_names,
            ttl.to_string(),
            IPVersions::IPv4,
        );

        {
            let mock_external_ip = create_external_ip_mock("127.1.2.3");
            let mock_listing = create_listing_request_mock(vec![]);
            let mock_add_v4 = create_add_request_mock("record1", "127.1.2.3", "ttl", "A");

            logic.update().unwrap();

            mock_external_ip.assert();
            mock_listing.assert();
            mock_add_v4.assert();
        }

        {
            let mock_external_ip = create_external_ip_mock("127.1.2.3");

            logic.update().unwrap();

            mock_external_ip.assert();
        }
    }

    #[test]
    fn test_change_on_separate_ip() {
        let record_names = vec![String::from("record1")];
        let ttl = "ttl";
        let froxlor = froxlor::Froxlor::new(
            mockito::server_url() + "/froxlor",
            API_KEY.to_string(),
            SECRET.to_string(),
        );
        let mut logic = super::Logic::new(
            froxlor,
            DOMAIN.to_string(),
            record_names,
            ttl.to_string(),
            IPVersions::IPv4,
        );

        {
            let mock_external_ip = create_external_ip_mock("127.1.2.3");
            let mock_listing = create_listing_request_mock(vec![]);
            let mock_add_v4 = create_add_request_mock("record1", "127.1.2.3", "ttl", "A");

            logic.update().unwrap();

            mock_external_ip.assert();
            mock_listing.assert();
            mock_add_v4.assert();
        }

        {
            let mock_external_ip = create_external_ip_mock("127.1.2.4");
            let mock_listing = create_listing_request_mock(vec![froxlor::Record {
                id: "7".to_string(),
                record_type: "A".to_string(),
            }]);
            let mock_add_v4 = create_add_request_mock("record1", "127.1.2.4", "ttl", "A");
            let mock_delete = create_delete_request_mock("7");

            logic.update().unwrap();

            mock_external_ip.assert();
            mock_listing.assert();
            mock_add_v4.assert();
            mock_delete.assert();
        }
    }
}
