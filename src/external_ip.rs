use std::io::Read;
use std::net::{Ipv4Addr, Ipv6Addr};

use crate::config::IPVersions;

#[cfg(test)]
use mockito;

/// Contains the external IPv4 and IPv6, depending on what was requested.
/// At least one Option will have Some value.
#[derive(Debug, PartialEq)]
pub struct ExternalIPs {
    pub ipv4: Option<Ipv4Addr>,
    pub ipv6: Option<Ipv6Addr>,
}

fn query_api(url: &str) -> Result<String, Box<dyn std::error::Error>> {
    let mut res = reqwest::blocking::get(url)?;
    let mut body = String::new();
    res.read_to_string(&mut body)?;

    if res.status() != 200 {
        return Err(body.into());
    }

    return Ok(body);
}

pub fn determine(ip_versions: &IPVersions) -> Result<ExternalIPs, Box<dyn std::error::Error>> {
    let ipv4 = match ip_versions {
        IPVersions::IPv4 | IPVersions::Both => {
            #[cfg(not(test))]
            let url = "https://api.ipify.org";
            #[cfg(test)]
            let url = &mockito::server_url();

            let response = query_api(url)?;
            response.parse().ok()
        }
        _ => None,
    };

    let ipv6 = match ip_versions {
        IPVersions::IPv6 | IPVersions::Both => {
            #[cfg(not(test))]
            let url = "https://api6.ipify.org";
            #[cfg(test)]
            let url = &mockito::server_url();

            let response = query_api(url)?;
            response.parse().ok()
        }
        _ => None,
    };

    return Ok(ExternalIPs {
        ipv4: ipv4,
        ipv6: ipv6,
    });
}

#[cfg(test)]
mod tests {
    use crate::config::IPVersions;
    use mockito::mock;
    use std::net::{Ipv4Addr, Ipv6Addr};

    #[test]
    fn test_success_v4() {
        let get_ip_request = mock("GET", "/")
            .with_status(200)
            .with_body("127.1.2.3")
            .create();

        let result = super::determine(&IPVersions::IPv4);

        assert!(result.is_ok());

        let ips = result.unwrap();

        get_ip_request.assert();

        assert!(ips.ipv4.is_some());
        assert_eq!(ips.ipv4.unwrap(), "127.1.2.3".parse::<Ipv4Addr>().unwrap());
        assert!(ips.ipv6.is_none());
    }

    #[test]
    fn test_success_v6() {
        let get_ip_request = mock("GET", "/")
            .with_status(200)
            .with_body("2a00:1450:400f:80d::200e")
            .create();

        let result = super::determine(&IPVersions::IPv6);

        assert!(result.is_ok());

        let ips = result.unwrap();

        get_ip_request.assert();

        assert!(ips.ipv4.is_none());
        assert!(ips.ipv6.is_some());
        assert_eq!(
            ips.ipv6.unwrap(),
            "2a00:1450:400f:80d::200e".parse::<Ipv6Addr>().unwrap()
        );
    }

    #[test]
    fn test_success_both() {
        let get_ip_request_v4 = mock("GET", "/")
            .with_status(200)
            .with_body("127.1.2.3")
            .create();

        let get_ip_request_v6 = mock("GET", "/")
            .with_status(200)
            .with_body("2a00:1450:400f:80d::200e")
            .create();

        let result = super::determine(&IPVersions::Both);

        assert!(result.is_ok());

        let ips = result.unwrap();

        get_ip_request_v4.assert();
        get_ip_request_v6.assert();

        assert!(ips.ipv4.is_some());
        assert_eq!(ips.ipv4.unwrap(), "127.1.2.3".parse::<Ipv4Addr>().unwrap());
        assert!(ips.ipv6.is_some());
        assert_eq!(
            ips.ipv6.unwrap(),
            "2a00:1450:400f:80d::200e".parse::<Ipv6Addr>().unwrap()
        );
    }

    #[test]
    fn test_error() {
        let get_ip_request = mock("GET", "/")
            .with_status(500)
            .with_body("server error")
            .create();

        let result = super::determine(&IPVersions::Both);

        get_ip_request.assert();

        assert!(result.is_err());
        assert!(result.unwrap_err().to_string() == "server error");
    }
}
