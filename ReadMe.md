# Dynamic DNS using Froxlor API

Auto-update the IP of DNS entries in the DNS editor of Froxlor for your domain.

You essentially keep this application running in the background and it will regularly check your current public IP using [ipify](https://www.ipify.org/). Whenever that IP changes it will use [Froxlor's API](https://api.froxlor.org/) to configure DNS records of your domain to point to that IP.

## in Docker

A minimal Docker image is built for this application. You can easily use it:

```
docker run -d \
  -e FROXLOR=https://your-server.com/froxlor \
  -e APIKEY=<your API key> \
  -e SECRET=<your API secret> \
  -e DOMAIN=your-site.com \
  -e RECORD_NAME=dyn \
  registry.gitlab.com/serviushack/froxlor-update-ip
```

This will update the IP of `dyn.your-site.com` every 10 minutes. The interval can be changed with the `INTERVAL` environment variable. It sets the number of seconds to wait between checks.

Instead of a single `RECORD_NAME` you can also provide a comma-separated list of records via `RECORD_NAMES`.

### IPv6

To get IPv6 support to call to `docker` from above must be modified slightly:

```
...
  -e IPV=both \
  --network host \
...
```

The `IPV` (short for IP versions) configuration controls for which type of IP addresses the records are updated. It takes one of these values:

* `4`: only the external IPv4 address is updated
* `6`: only the external IPv6 address is updated
* `both`: the external IPv4 address and the external IPv6 address are updated

The host network is necessary for almost all Docker installations since Docker doesn't support IPv6 by default.
